package com.binaryserch;

public class BinarySearch {
	/*
	*此方法通过二分法用来查找指定的key值
	*输入：输入一个待查找的数组和待查找的Key值
	*返回:  1.如果找到，返回key值所对应的下标
	*      2.如果没有找到，返回-1.
	*/
	public int binarySearch(int [] data,int key){
		int low = 0;
		int high = data.length-1;
		int mid = 0;
		while(low <= high){
			mid = (low + high)/2;
			if(data[mid] < key){
				low = mid+1;
			}else if(data[mid] >key){
				high = mid-1;
			}else{
				return mid;
			}
		}
		return -1;
	}
	
	
	/*
	*此方法用来检测输入的数组是否是升序有序的
	*输入：输入一个待判断的数组
	*返回:  1.如果有序，则返回true
	*      2.如果无序，则返回false
	*/
	public boolean checkOutArray(int [] data){
		int i ;
		boolean flag = true;
		for(i = 1;i < data.length;i++){
			if(data[i] < data[i-1]) {
				flag = false;
				break;
			}
		}
		return flag;
	}
	public static void main(String[] args) {
		int [] array = {1,2,3,9,27,31,66,100};
		int key_value = 1000;
		BinarySearch binary = new BinarySearch();
		if( array != null && array.length > 0){
			  if(binary.checkOutArray(array) == true){
				  int index = binary.binarySearch(array,key_value);
				  if(index != -1){
				  	 System.out.println("key对应的下标:"+index);
				  }else{
					 System.out.println("序列中无要查找的key值!!!!");
				  }
				 
			  }else{
				  	 System.out.println("输入非升序序列!!!!");
				 }
			  
		}else{
			System.out.println("数据输入有误!!!!");
		}
		
	}
}
