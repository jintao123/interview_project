## (function($, export){})(jQuery, window)的理解;
    export应该是JavaScript中的关键字，所以我改为extend
    这种代码写法，应该jQuery写插件的书写方法。
    function($,extend)
    这两个个参数的意思是：
        $  是把jQuery传进来，这样你可以在function中可以继续使用$作为jQuery的引用
        export  把一个当前的外部对象传入，以便在插件中继续使用整条语句应该是定义了一个juqery插件，并传入了jquery和
        window对象且立即调用，或者也可以在声明的函数中把定义的方法或属性给添加到实参上，这样在函数外部也可以通过实参
        进行对其的使用，也就是扩展了jquery
        
        
## 前端框架的理解

    由于只使用过jquery，所以在此只能谈谈对jquery的理解jQuery的定位就是把JavaScript封装起来，形成一个轻量级的，更加
    兼容的dom操作库，当然，只用dom操作库还是根本不足以概括jquery的强大，它的作用非常华丽，对事件，动画，等等都有很好的支持
    优点：
        1.jquery是开源的一个插件
        2.jQuery消除了JavaScript跨平台兼容问题。
        3.相比其他JavaScript和JavaScript库，jQuery更容易使用。
        4.jQuery有一个庞大的库/函数。
        5.jQuery有良好的文档和帮助手册。
        6.jQuery支持AJAX。
    缺点:
        1.jquery相对于原生的JavaScript理解起来很难，用起来容易，理解吃透很难。
        2.jquery的各种版本的兼容性很差，不能向后兼容
        3.项目中需要包含jQuery库文件。如果包含多个版本的jQuery库，可能会发生冲突。