## HashMap基于哈希表的 Map 接口的实现。
    此实现提供所有可选的映射操作，并允许使用 null值和 null键。（除了不同步和允许使用 null 之外，HashMap 
    类与 Hashtable 大致相同。）此类不保证映射的顺序，特别是它不保证该顺序恒久不变。值得注意的是HashMap
    不是线程安全的，如果想要线程安全的HashMap，可以通过Collections类的静态方法synchronizedMap获得线程安全的HashMap。
        Map map = Collections.synchronizedMap(new HashMap());

## HashMap的数据结构
    HashMap的底层是通过数组加链表实现的，它的查询速度相当快是因为它通过计算散列码来决定存储的位置，也能够
    通过散列码来快速的计算出对象所存储的位置，HashMap中主要是通过key的hashCode来计算hash值的，只要hashCode
    相同，计算出来的hash值就一样。如果存储的对象对多了，就有可能不同的对象所算出来的hash值是相同的，这就出现
    了所谓的hash冲突。解决hash冲突的方法有很多，HashMap底层是通过链表来解决hash冲突的。
## HashMap的存取过程
    put方法:
        1.判断key是否为空，如果是的话，调用putForNullKey（value）（如果key为null的话，hash值为0，对象存储在数组中索引为0的位置。）
        2.根据key值的hashCode（）方法得到hashcode，然后再通过hash（）方法计算hash码
        3.再通过indexFor（）方法，根据hash码和数组table的长度计算数组的索引
        4.再将entry对象插入到table数组中（如果key相同则覆盖并返回旧值），再看有无hash冲突再判断是否使用链表解决冲突。
    get方法:
        当你传递一个key从hashmap总获取value的时候：
        1.对key进行null检查。如果key是null，table[0]这个位置的元素将被返回。
        2.key的hashcode()方法被调用，然后计算hash值。
        3.indexFor(hash,table.length)用来计算要获取的Entry对象在table数组中的精确的位置，使用刚才计算的hash值。
        4.在获取了table数组的索引之后，会迭代链表，调用equals()方法检查key的相等性，如果equals()方法返回true，
            get方法返回Entry对象的value，否则，返回null。
## HashMap的优缺点
    应用场景:
        HashMap里面存入的键值对在取出的时候是随机的,它根据键的HashCode值存储数据,根据键可以直接获取它的值，
    具有很快的访问速度。在Map 中插入、删除和定位元素，HashMap 是最好的选择。 
        1.我觉得HashMap适用于数据量比较大的，而且查找，存取较多，不考虑排序的情况下
        2.HashMap也适用于多线程的程序中，因为虽然HashMap是非线程安全的，不是同步的，但是可以通过Collections中的方法使HashMap同步。
        3.HashMap相对来说当已知元素个数时，或个数区间时，适用HashMap，因为HashMap扩容非常影响性能。
    优点：
        超级快速的查询速度，HashMap的查询速度可以达到O(1)
        动态的可变长存储数据（和数组相比较而言）
        可适用于处理海量的数据的存储和查找
    缺点：
        需要额外计算一次hash值
        如果处理不当会占用额外的空间
        不是线程安全的 
## Map与JavaBean的优缺点
    JavaBean
        提高代码的可读性，易维护
        有get/set操作，可对数据进行一些必要的操作。
        还有JavaBean中的属性和字段可以添加说明注释，类型也是固定的。
    map
        相对于JavaBean来说，效率更高点，因为JavaBean应用了反射，反射会降低效率。
        map可以动态的添加属性字段，且不用修改业务层的代码。相对来说，容易扩展。